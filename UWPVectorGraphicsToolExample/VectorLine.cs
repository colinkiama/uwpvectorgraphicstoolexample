﻿using Microsoft.Graphics.Canvas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI;

namespace UWPVectorGraphicsToolExample
{
    public class VectorLine:ICanvasDrawable
    {
        public float StrokeThickness { get; set; } = 1.0f;
        public Vector2 StartPoint{ get; set; }
        public Vector2 EndPoint { get; set; }

        public void Draw(CanvasDrawingSession canvasDrawingSession, Size canvasSize, Vector2 parentPosition)
        {
            // 1. Calculate actual positions of points
            Vector2 StartPointInWorldSpace = StartPoint + parentPosition;
            Vector2 EndPointInWorldSpace = EndPoint + parentPosition;
            canvasDrawingSession.DrawLine(StartPointInWorldSpace, EndPointInWorldSpace, Colors.Blue);
        }
    }
}
