﻿using Microsoft.Graphics.Canvas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;

namespace UWPVectorGraphicsToolExample
{
    public interface ICanvasDrawable
    {
        void Draw(CanvasDrawingSession canvasDrawingSession, Size canvasSize, System.Numerics.Vector2 parentPosition);
    }
}
