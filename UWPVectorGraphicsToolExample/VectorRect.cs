﻿using Microsoft.Graphics.Canvas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI;

namespace UWPVectorGraphicsToolExample
{
    public class VectorRect : ICanvasDrawable
    {
        public Vector2 Position { get; set; }
        public float Width { get; set; }
        public float Height { get; set; }

       
        public void Draw(CanvasDrawingSession canvasDrawingSession, Size canvasSize, Vector2 parentPosition)
        {
            Vector2 rectPositionInWorldSpace = parentPosition + Position;
            
            canvasDrawingSession.DrawRectangle(rectPositionInWorldSpace.X, rectPositionInWorldSpace.Y, Width, Height, Colors.Purple);
        }
    }
}
