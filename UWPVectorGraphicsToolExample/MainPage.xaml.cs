﻿using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.Geometry;
using Microsoft.Graphics.Canvas.Svg;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI;
using Windows.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace UWPVectorGraphicsToolExample
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// Adapted from Win2D "Mandlebrot" Example: https://github.com/microsoft/Win2D-Samples/blob/master/ExampleGallery/Mandelbrot.xaml.cs 
    /// </summary>
    public sealed partial class MainPage : Page
    {
        ArtBoard artBoard = new ArtBoard(400, 400, new Vector2(0, 0));
        List<ICanvasDrawable> GlobalBoard = new List<ICanvasDrawable>();
        float displayDpi;
        bool isPointerBeingDragged = false;


        public MainPage()
        {
            this.InitializeComponent();
            SubscribeToPointerEvents();
            NothingRadioButton.IsChecked = true;
            MouseEventService.Instance.PointerReleasedWithValidPoints += Instance_PointerReleasedWithValidPoints;
            MouseEventService.Instance.PointerMovedWithValidPoints += Instance_PointerMovedWithValidPoints;
            MouseEventService.Instance.PointerDraggingEnded += Instance_PointerDraggingEnded;
        }

        private void Instance_PointerDraggingEnded(object sender, EventArgs e)
        {
            isPointerBeingDragged = false;
            VirtualCanvas.Invalidate();
        }

        private void Instance_PointerMovedWithValidPoints(object sender, EventArgs e)
        {
            isPointerBeingDragged = true;
            VirtualCanvas.Invalidate();
        }

        private void Instance_PointerReleasedWithValidPoints(object sender, EventArgs e)
        {

            Point[] mouseEventPoints = MouseEventService.Instance.GetMouseEventPoints();
            var rect = new Rect(mouseEventPoints[0], mouseEventPoints[1]);
            VectorRect rectToAdd = new VectorRect
            {
                Position = new Vector2((float)rect.X, (float)rect.Y),
                Width = (float)rect.Width,
                Height = (float)rect.Height
            };

            Size canvasSize = VirtualCanvas.Size;
            if (artBoard.CanContainElement(rect, canvasSize))
            {
                artBoard.AddElementFromWorldSpace(rectToAdd, canvasSize);

            }
            else
            {
                GlobalBoard.Add(rectToAdd);
            }
            MouseEventService.Instance.ClearMouseEventPoints();

            VirtualCanvas.Invalidate();
        }

        private void SubscribeToPointerEvents()
        {
            VirtualCanvas.PointerPressed += MouseEventService.Instance.VirtualCanvas_PointerPressed;
            VirtualCanvas.PointerReleased += MouseEventService.Instance.VirtualCanvas_PointerReleased;
            VirtualCanvas.PointerMoved += MouseEventService.Instance.VirtualCanvas_PointerMoved;
            VirtualCanvas.PointerCanceled += MouseEventService.Instance.VirtualCanvas_PointerCanceled;
            VirtualCanvas.PointerCaptureLost += MouseEventService.Instance.VirtualCanvas_PointerCaptureLost;
        }



        private void UnsubsribeToPointerEvents()
        {
            VirtualCanvas.PointerEntered -= MouseEventService.Instance.VirtualCanvas_PointerPressed;
            VirtualCanvas.PointerReleased -= MouseEventService.Instance.VirtualCanvas_PointerReleased;
            VirtualCanvas.PointerMoved -= MouseEventService.Instance.VirtualCanvas_PointerMoved;
            VirtualCanvas.PointerCanceled -= MouseEventService.Instance.VirtualCanvas_PointerCanceled;
            VirtualCanvas.PointerCaptureLost -= MouseEventService.Instance.VirtualCanvas_PointerCaptureLost;
        }

        private void VirtualCanvas_RegionsInvalidated(Microsoft.Graphics.Canvas.UI.Xaml.CanvasVirtualControl sender, Microsoft.Graphics.Canvas.UI.Xaml.CanvasRegionsInvalidatedEventArgs args)
        {
            var regionsToDraw = args.InvalidatedRegions;
            for (int i = 0; i < regionsToDraw.Length; i++)
            {
                Rect region = regionsToDraw[i];
                using (CanvasDrawingSession drawingSession = sender.CreateDrawingSession(region))
                {
                    // TODO: Check if artboards or any other elements are in the invalidated regions before
                    // trying to draw them.
                    //
                    // Also it would be a good idea to draw only a part of an element instead 
                    // of the whole element if the whole element
                    // is not included in the region to draw.


                    Size canvasSize = sender.Size;
                    // Enables smooth edges
                    drawingSession.Antialiasing = CanvasAntialiasing.Antialiased;
                    artBoard.Draw(drawingSession, sender.Size);
                    foreach (var item in GlobalBoard)
                    {
                        item.Draw(drawingSession, canvasSize, Vector2.Zero);
                    }

                    if (isPointerBeingDragged)
                    {
                        Point[] dragEventPoints = MouseEventService.Instance.GetMouseEventPoints();
                        Rect draggingAreaRect = new Rect(dragEventPoints[0], dragEventPoints[1]);
                        drawingSession.DrawRectangle(draggingAreaRect, Colors.Aqua);
                        isPointerBeingDragged = false;
                    }

                }
            }
        }





        private void VirtualCanvas_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            VirtualCanvas.Invalidate();
        }

        private void CanvasScrollViewer_ViewChanged(object sender, ScrollViewerViewChangedEventArgs e)
        {
            // Cancel out the display DPI, so vector graphics always render at 96 DPI regardless of display
            // configuration. This boosts performance on high DPI displays, at the cost of visual quality.
            // For even better performance (but lower quality) this value could be further reduced.

            // Set to 1.0f to always use display DPI (at the cost of performance in some cases.)
            float dpiAdjustment = 96 / displayDpi;

            // Adjust DPI to match the current zoom level.
            float dpiScale = dpiAdjustment * CanvasScrollViewer.ZoomFactor;

            // To boost performance during pinch-zoom manipulations, we only update DPI when it has
            // changed by more than 20%, or at the end of the zoom (when e.IsIntermediate reports false).
            // Smaller changes will just scale the existing bitmap, which is much faster than recomputing
            // the fractal at a different resolution. To trade off between zooming perf vs. smoothness,
            // adjust the thresholds used in this ratio comparison.
            var ratio = VirtualCanvas.DpiScale / dpiScale;

            if (e == null || !e.IsIntermediate || ratio <= 0.8 || ratio >= 1.25)
            {
                VirtualCanvas.DpiScale = dpiScale;
            }
        }


        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            // Initialize the display DPI, and listen for events in case this changes.
            var display = DisplayInformation.GetForCurrentView();
            display.DpiChanged += Display_DpiChanged;
            Display_DpiChanged(display, null);
        }


        void Display_DpiChanged(DisplayInformation sender, object args)
        {
            displayDpi = sender.LogicalDpi;

            // Manually call the ViewChanged handler to update DpiScale.
            CanvasScrollViewer_ViewChanged(null, null);
        }

        private void CanvasScrollViewer_Loaded(object sender, RoutedEventArgs e)
        {
            Size canvasSize = VirtualCanvas.Size;
            //// Starting scrollviewer so that it's centered on the artboard.
            // Note: "Origin" is located at the center of the canvas
            CanvasScrollViewer.ChangeView(0.5 * canvasSize.Width - CanvasScrollViewer.ActualWidth / 2, 0.5 * canvasSize.Height - CanvasScrollViewer.ActualHeight / 2, null);
        }

        private void Page_Unloaded(object sender, RoutedEventArgs e)
        {
            this.VirtualCanvas.RemoveFromVisualTree();
            this.VirtualCanvas = null;
        }

        private void InteractionModeRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            var radioButton = (RadioButton)sender;
            string tagName = radioButton.Tag.ToString();
            switch (tagName)
            {
                case "nothing":
                    // Stop listening for mouse events
                    // Clear all previous data you had on points
                    UnsubsribeToPointerEvents();
                    MouseEventService.Instance.ClearMouseEventPoints();
                    break;
                case "rectangle":
                    // Start listening for mouse events
                    // Read clicks, drags and releases 
                    // to draw rectangles
                    SubscribeToPointerEvents();
                    break;
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearElementsFromCanvas();
        }

        private void ClearElementsFromCanvas()
        {
            GlobalBoard.Clear();
            artBoard.Clear();
            VirtualCanvas.Invalidate();
        }
    }
}

