﻿using Microsoft.Graphics.Canvas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI;
using Windows.UI.Xaml.Shapes;

namespace UWPVectorGraphicsToolExample
{
    public class ArtBoard
    {
        public float Width { get; set; }
        public float Height { get; set; }
        public Vector2 Position { get; set; }

        private List<ICanvasDrawable> ItemCollection { get; set; } = new List<ICanvasDrawable>();

        public void AddElement(ICanvasDrawable elementToAdd)
        {
            ItemCollection.Add(elementToAdd);
        }

        public void AddElementFromWorldSpace(ICanvasDrawable worldSpaceElementToAdd, Size canvasSize)
        {
            Vector2 artBoardInWorldSpacePosition = CalulateArtBoardWorldSpacePosition(canvasSize);
            if (worldSpaceElementToAdd is VectorRect rectToAdd)
            {
                Vector2 worldSpacePosition = rectToAdd.Position;
                rectToAdd.Position = worldSpacePosition - artBoardInWorldSpacePosition;
                ItemCollection.Add(rectToAdd);
            }
        }

        public ArtBoard(float width, float height, Vector2 relativePosition)
        {
            Width = width;
            Height = height;
            Position = relativePosition;

            // Testing if line draws
            ItemCollection.Add(new VectorLine
            {
                StartPoint = new Vector2(0, 0),
                EndPoint = new Vector2(50, 50)
            });

            ItemCollection.Add(new VectorRect
            {
                Position = new Vector2(20, 20),
                Width = 50,
                Height = 100
            });
        }
        public void Draw(CanvasDrawingSession canvasDrawingSession, Size canvasSize)
        {
            // 1. Calculate the Artbords's position in world-space
           
            Vector2 artBoardPositionInWorldSpace = CalulateArtBoardWorldSpacePosition(canvasSize);
            

            canvasDrawingSession.DrawRectangle(artBoardPositionInWorldSpace.X, artBoardPositionInWorldSpace.Y, Width, Height, Colors.Black);
            canvasDrawingSession.FillRectangle(artBoardPositionInWorldSpace.X, artBoardPositionInWorldSpace.Y, Width, Height, Colors.White);

            for (int i = 0; i < ItemCollection.Count; i++)
            {
                ItemCollection[i].Draw(canvasDrawingSession, canvasSize, artBoardPositionInWorldSpace);
            }
        }

        private Vector2 CalulateArtBoardWorldSpacePosition(Size canvasSize)
        {
            var worldOriginVector = CalculateworldOriginVector(canvasSize);
            return worldOriginVector + Position - new Vector2(Width, Height) / 2;
        }

        private Vector2 CalulateArtBoardWorldSpacePosition(Vector2 worldOriginVector)
        {
            return worldOriginVector + Position - new Vector2(Width, Height) / 2;
        }

        internal bool CanContainElement(Rect rectToAdd, Size canvasSize)
        {
            bool canArtBoardContainElement = false;
            Vector2 worldOriginVector = CalculateworldOriginVector(canvasSize);
            Vector2 artBoardPositionInWorldSpace = CalulateArtBoardWorldSpacePosition(worldOriginVector);

            
            Rect artBoardRect = new Rect(artBoardPositionInWorldSpace.X, artBoardPositionInWorldSpace.Y, Width, Height);

            var artBoardRectCopy = new Rect(artBoardRect.X, artBoardRect.Y, artBoardRect.Width, artBoardRect.Height);
            // Note: rectToAdd's position is already in world-space in this situation.
            
            // If the "expanded" rect from the union operation is equal to the original
            // art board rect, that means that no expansion was needed
            // therefore, the element can be added into the board!
            artBoardRectCopy.Union(rectToAdd);
            if (artBoardRectCopy == artBoardRect)
            {
                canArtBoardContainElement = true;
            }
            
            return canArtBoardContainElement;
        }

        private Vector2 CalculateworldOriginVector(Size canvasSize)
        {
            return new Vector2((float)(canvasSize.Width / 2), (float)(canvasSize.Height / 2));
        }

        public void Clear()
        {
            ItemCollection.Clear();
        }
    }
}
