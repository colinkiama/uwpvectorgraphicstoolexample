﻿using Microsoft.Graphics.Canvas.UI.Xaml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI.Input;
using Windows.UI.Xaml.Input;

namespace UWPVectorGraphicsToolExample
{
    public sealed class MouseEventService
    {
        private static Lazy<MouseEventService> lazy = new Lazy<MouseEventService>(() => new MouseEventService());
        public event EventHandler PointerReleasedWithValidPoints;
        public event EventHandler PointerMovedWithValidPoints;
        public event EventHandler PointerDraggingEnded;
        public static MouseEventService Instance = lazy.Value;
        private Point[] mouseEventPoints = new Point[2];

        bool _isDragging = false;

        public Point[] GetMouseEventPoints()
        {
            return mouseEventPoints;
        }

        public void SetMouseEventPoints(Point[] value)
        {
            mouseEventPoints = value;
        }

        private MouseEventService()
        {
            ClearMouseEventPoints();
        }

        public void ClearMouseEventPoints()
        {
            GetMouseEventPoints()[0] = new Point(double.NaN, double.NaN);
            GetMouseEventPoints()[1] = new Point(double.NaN, double.NaN);
            _isDragging = false;
            PointerDraggingEnded?.Invoke(this, EventArgs.Empty);
        }

        private void AddToMouseEventEndPoint(PointerPoint pointerPoint)
        {
            GetMouseEventPoints()[1] = pointerPoint.Position;
        }

        private void AddToMouseEventStartPoint(PointerPoint pointerPoint)
        {
            GetMouseEventPoints()[0] = pointerPoint.Position;
        }

        private bool CheckIfMouseEventPointsAreValid()
        {
            bool arePointsValid = true;
            foreach (var point in GetMouseEventPoints())
            {
                arePointsValid = !(double.IsNaN(point.X) && double.IsNaN(point.Y));
            }
            return arePointsValid;
        }

        internal void VirtualCanvas_PointerReleased(object sender, PointerRoutedEventArgs e)
        {
            var virtualCanvas = (CanvasVirtualControl)sender;
            AddToMouseEventEndPoint(e.GetCurrentPoint(virtualCanvas));

            if (CheckIfMouseEventPointsAreValid())
            {
                PointerReleasedWithValidPoints?.Invoke(this, EventArgs.Empty);
            }
            _isDragging = false;
        }

        internal void VirtualCanvas_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            var virtualCanvas = (CanvasVirtualControl)sender;
            AddToMouseEventStartPoint(e.GetCurrentPoint(virtualCanvas));
            _isDragging = true;
        }



        internal void VirtualCanvas_PointerCaptureLost(object sender, PointerRoutedEventArgs e)
        {
            ClearMouseEventPoints();
        }

        internal void VirtualCanvas_PointerCanceled(object sender, PointerRoutedEventArgs e)
        {
            ClearMouseEventPoints();
        }


        internal void VirtualCanvas_PointerMoved(object sender, PointerRoutedEventArgs e)
        {
            var virtualCanvas = (CanvasVirtualControl)sender;
            AddToMouseEventEndPoint(e.GetCurrentPoint(virtualCanvas));

            if (CheckIfMouseEventPointsAreValid() && _isDragging)
            {
                PointerMovedWithValidPoints?.Invoke(this, EventArgs.Empty);
                
            }
        }

    }
}
