# UWP Vector Graphics Tool Example

This is a very basic demo of UWP's potential as an app framework for a Vector Graphics Editor.

## Features
- Smooth, responsive scrolling and zooming (even touch gestures)
- Artboard
- Line Drawing
- Rectangle drawing invoked by user
- Zoom with quality always maintained
- Ability to clear drawn elements

## A Warning
This is a **very** basic demo. There is no select tool but you could probably implement that yourself by adapting this code.
![](img/screenshot.jpg)